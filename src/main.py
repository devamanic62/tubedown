import click
from pytube import YouTube


def download_from_youtube(url: str):
    YouTube(url).streams.first().download()


@click.command()
@click.option("--url", default="https://www.youtube.com/watch?v=lTTajzrSkCw")
def main(url):
    download_from_youtube(url)


if __name__ == '__main__':
    main()
