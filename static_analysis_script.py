import re
import sys

def check_pylint_score(file_path, threshold):
    with open(file_path, 'r') as file:
        content = file.read()
    
    match = re.search(r'Your code has been rated at ([0-9\.]+)/10', content)
    if match:
        score = float(match.group(1))
        if score < threshold:
            print(f"Pylint score {score} is below the acceptable threshold of {threshold}.")
            sys.exit(1)
        else:
            print(f"Pylint score {score} meets the acceptable threshold of {threshold}.")
    else:
        print("Pylint score not found.")
        sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python check_pylint.py <pylint_output_file> <threshold>")
        sys.exit(1)
    
    pylint_output_file = sys.argv[1]
    threshold = float(sys.argv[2])
    
    check_pylint_score(pylint_output_file, threshold)