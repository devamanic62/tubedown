from setuptools import setup, find_packages

setup(
    name='tubedown',
    version='1.0.0',
    packages=find_packages(),  # Automatically find packages in the current directory
    install_requires=[
        'click==8.1.7',
        'pytube==15.0.0',
        'pylint==3.2.5',
        'pylint-json2html==0.5.0'
    ],
    # entry_points={s
    #     'console_scripts': [
    #         'your_command = your_package.module:main_function',  # Define command-line entry points
    #     ],
    # },
    author='deva',
    author_email='your_email@example.com',
    description='youtube video downloader',
    license='MIT',
    keywords='youtube downloader',
    url='https://gitlab.com/devamanic62/tubedown',  # URL to your project homepage
)
